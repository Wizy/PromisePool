module.exports = function(originalTasks, limit) {
    return new Promise((resolve, reject) => {
        let tasks = originalTasks.map((task, i) => {
            return {
                index: i,
                task: task,
            };
        });
        let results = new Array(tasks.length);
        let done = 0;
        let runningCount = 0;
        let error = false;

        function run() {
            if(tasks.length > 0 && error === false) {
                if(runningCount < limit || !limit) {
                    // pick first non started task
                    const task = tasks.shift();
                    runningCount++;

                    // start it
                    task.task().then((result) => {
                        // task done
                        results[task.index] = result;
                        done++;
                        runningCount--;
                        setTimeout(run, 0);
                    }).catch((e) => {
                        error = true;
                        reject(e);
                    });
                    setTimeout(run, 0);
                }
            }

            // finished
            if(done === originalTasks.length) {
                resolve(results);
            }
        }
        run();
    });
};

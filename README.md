## Introduction

Gives an alternative to ES Promise.all which uses a pool to limit the number of concurrent running promises.

## Example
```
const PromisePool = require('promise-pool');
const tasks = new Array(10);
for(let i = 0; i < tasks.length; i++) {
    tasks[i] = async function() {
        await sleep(1000);
    };
}
await PromisePool(tasks, 2);
```
Will run only two tasks at a time.

const assert = require('assert');
const PromisePool = require('../main');

function sleep(duration) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, duration);
    });
}

describe('PromisePool', function() {
    it('should work with empty list', async function() {
        const result = await PromisePool([]);
        assert(Array.isArray(result));
        assert.equal(0, result.length);
    });
    it('should work with one promise', async function() {
        const task = async (resolve) => {
            await sleep(100);
            return 42;
        };
        const result = await PromisePool([task]);
        assert(Array.isArray(result));
        assert.equal(1, result.length);
        assert.equal(42, result[0]);
    });
    it('should keep order with many promises', async function() {
        let tasks = [];
        for(let i = 0; i < 42; i++) {
            tasks.push(async (resolve) => {
                await sleep(100);
                return i;
            });
        }
        const result = await PromisePool(tasks);

        assert(Array.isArray(result));
        assert.equal(42, result.length);
        for(let i = 0; i < 42; i++) {
            assert.equal(result[i], i);
        }
    });
    it('should keep order with many promises, limit', async function() {
        const count = 42, simultaneous = 4;
        let tasks = [];
        let running = 0,
            finished = 0;
        let runningAverage = 0,
            runningMax = 0;

        for(let i = 0; i < count; i++) {
            tasks.push(async (resolve) => {
                running++;
                runningAverage += running;
                if(running > runningMax)
                    runningMax = running;

                await sleep(100);
                running--;
                finished++;

                return i;
            });
        }

        const result = await PromisePool(tasks, simultaneous);

        runningAverage /= count;
        assert(Array.isArray(result));
        assert.equal(count, result.length);
        for(let i = 0; i < count; i++) {
            assert.equal(result[i], i);
        }
        assert.equal(runningMax, simultaneous);
        assert(runningAverage > 1);
        assert(runningAverage < simultaneous);
    });
    it('should fail', async function() {
        const count = 42, simultaneous = 4;
        let tasks = [];
        let running = 0,
            finished = 0;
        let runningAverage = 0,
            runningMax = 0;
        let result;

        for(let i = 0; i < count; i++) {
            tasks.push(async (resolve) => {
                running++;
                runningAverage += running;
                if(running > runningMax)
                    runningMax = running;

                if(i === count / 2)
                    throw 'Expected exception';

                await sleep(100);
                running--;
                finished++;

                return i;
            });
        }

        try {
            result = await PromisePool(tasks, simultaneous);
            throw 'Should have thrown exception';
        }
        catch(e) {
            assert.equal(e, 'Expected exception');

            runningAverage /= count;
            assert.equal(result, undefined);
            assert.equal(runningMax, simultaneous);
            assert(runningAverage > 1);
            assert(runningAverage < simultaneous);
            assert(finished > count / 2 - simultaneous);
            assert(finished < count / 2);
        }
    });
});
